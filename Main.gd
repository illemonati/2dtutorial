extends Node

@export var mob_scene: PackedScene
@export var mob_velocity_min = 150.0
@export var mob_velocity_max = 250.0

var score

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func game_over():
	$HUD.show_game_over()
	$ScoreTimer.stop()
	$MobTimer.stop()
	$Music.stop()
	$DeathSound.play()
	
func new_game():
	score = 0
	$HUD.update_score(score)
	$HUD.show_message("It Begins")
	$Player.start($StartPosition.position)
	get_tree().call_group("mobs", "queue_free")
	$StartTimer.start()
	$Music.play()


func _on_mob_timer_timeout():
	var mob = mob_scene.instantiate()
	var mob_spawn_location = $MobPath/MobSpawnLocation
	mob_spawn_location.progress_ratio  = randf()
	
	mob.rotation = mob_spawn_location.rotation + PI / 2
	mob.position = mob_spawn_location.position
	
	mob.rotation += randf_range(-PI/4, PI/4)
	
	var velocity = Vector2(randf_range(mob_velocity_min, mob_velocity_max), 0.0)
	mob.linear_velocity = velocity.rotated(mob.rotation)
	
	add_child(mob)

func _on_score_timer_timeout():
	score += 1
	$HUD.update_score(score)


func _on_start_timer_timeout():
	$MobTimer.start()
	$ScoreTimer.start()
